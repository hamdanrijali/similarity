package com.gdt.similaritymims;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimilarityMimsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimilarityMimsApplication.class, args);
    }

}
