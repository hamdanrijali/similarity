package com.gdt.similaritymims.print;

import com.gdt.similaritymims.dto.GenerateFreemarkerDto;
import freemarker.template.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.util.Map;

@Service("printService")
public class PrintServiceImpl implements PrintService {

    private final Configuration fmConfiguration;

    public PrintServiceImpl(Configuration fmConfiguration) {
        this.fmConfiguration = fmConfiguration;
    }

    public String generateMims(GenerateFreemarkerDto freemarkerDto) {
        return getTemplatePrintPnrView(freemarkerDto.getModel());
    }

    private String getTemplatePrintPnrView(Map<String, Object> model) {
        StringBuilder content = new StringBuilder();

        try {
            content.append(FreeMarkerTemplateUtils.processTemplateIntoString(fmConfiguration.getTemplate("combine-mims.txt"), model));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return content.toString();
    }
}
