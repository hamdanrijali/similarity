package com.gdt.similaritymims.print;

import com.gdt.similaritymims.dto.GenerateFreemarkerDto;

public interface PrintService {
    String generateMims(GenerateFreemarkerDto freemarkerDto);
}
