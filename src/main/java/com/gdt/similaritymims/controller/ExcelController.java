package com.gdt.similaritymims.controller;

import com.gdt.similaritymims.dto.GenerateFreemarkerDto;
import com.gdt.similaritymims.dto.MimsDto;
import com.gdt.similaritymims.dto.MimsImagesDto;
import com.gdt.similaritymims.print.FreemarkerConfig;
import com.gdt.similaritymims.print.PrintService;
import info.debatty.java.stringsimilarity.JaroWinkler;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class ExcelController {

    @PostMapping("/similarity")
    public void mapReapExcelDatatoDB(@RequestParam() BigDecimal threshold, @RequestParam() MultipartFile currentFile, @RequestParam() MultipartFile databaseDrugsFile) throws IOException {

        List<MimsDto> mimsDtos = new ArrayList<>();
        XSSFWorkbook workbookCurrent = new XSSFWorkbook(currentFile.getInputStream());
        XSSFSheet worksheetCurrent = workbookCurrent.getSheetAt(0);

        XSSFWorkbook workbookMims = new XSSFWorkbook(databaseDrugsFile.getInputStream());
        XSSFSheet worksheetMims = workbookMims.getSheetAt(0);

        JaroWinkler jaroWinkler = new JaroWinkler();

        for (int i = 1; i < worksheetCurrent.getPhysicalNumberOfRows(); i++) {
            MimsDto mimsDto = new MimsDto();
            List<String> rowNumMims = new ArrayList<>();

            XSSFRow rowCurrent = worksheetCurrent.getRow(i);
            System.out.println("ROW KE: " + i);

            String productNameCurrent = rowCurrent.getCell(0).getStringCellValue().toLowerCase();
            if (productNameCurrent.equals("")) {
                continue;
            }
            String similarityPercent = "Need To Check or Not Found in Mims";
            BigDecimal tempSililarityDec = BigDecimal.ZERO;
//            for (int j = 0; j < MIMS_DATA.size(); j++) {
            for (int j = 0; j < worksheetMims.getPhysicalNumberOfRows(); j++) {
                XSSFRow rowMims = worksheetMims.getRow(j);
                String productNameMims = rowMims.getCell(0).getStringCellValue().toLowerCase();
//                String productNameMims = MIMS_DATA.get(j).toLowerCase();
//                StringsComparator cmp = new StringsComparator(productNameCurrent.toLowerCase(), productNameMims.toLowerCase());
//                EditScript<Character> script = cmp.getScript();
//                BigDecimal similarityDec = new BigDecimal(script.getModifications());

                BigDecimal similarityDec = new BigDecimal(jaroWinkler.similarity(productNameCurrent, productNameMims)).setScale(2, RoundingMode.DOWN);

                if (similarityDec.compareTo(threshold) >= 0) {
                    if (similarityDec.compareTo(tempSililarityDec) > 0) {
                        similarityPercent = similarityDec.multiply(new BigDecimal(100)) + "%";
                        tempSililarityDec = similarityDec;
                    }
                    rowNumMims.add((similarityDec.multiply(new BigDecimal(100)) + "%").concat("-").concat(productNameMims).concat("-").concat(String.valueOf(j + 1)));
                }
            }

            mimsDto.setProductName(productNameCurrent);
            mimsDto.setSimilarity(similarityPercent);
            rowNumMims = rowNumMims.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
            mimsDto.setRowMims(rowNumMims.toString());

            mimsDtos.add(mimsDto);
        }

        Workbook wb = new HSSFWorkbook();
        OutputStream fileOut = new FileOutputStream("similarity.xls");

        Sheet sheet = wb.createSheet("Similarity_Result");
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Nama_sp");
        header.createCell(1).setCellValue("Highest Similarity");
        header.createCell(2).setCellValue("Product Mims (desc:{similarity}-{product_name}-{row_in_mims}");
        int counter = 1;

        for (MimsDto mims : mimsDtos) {
            Row row = sheet.createRow(counter++);
            row.createCell(0).setCellValue(mims.getProductName());
            row.createCell(1).setCellValue(mims.getSimilarity());
            row.createCell(2).setCellValue(mims.getRowMims());
        }

        wb.write(fileOut);
    }

    @PostMapping("/combine/picture")
    public void combinePicture(@RequestParam() String pathImages, @RequestParam() MultipartFile excelFile) throws IOException {

        XSSFWorkbook workbookExcelFile = new XSSFWorkbook(excelFile.getInputStream());
        XSSFSheet worksheetExcelFile = workbookExcelFile.getSheetAt(0);

        AbstractApplicationContext context = new AnnotationConfigApplicationContext(FreemarkerConfig.class);
        PrintService printService = (PrintService) context.getBean("printService");
        GenerateFreemarkerDto generateFreemarkerDto = new GenerateFreemarkerDto();

        List<MimsImagesDto> mimsImagesDtoList = new ArrayList<>();
        for (int i = 1; i < worksheetExcelFile.getPhysicalNumberOfRows(); i++) {
            MimsImagesDto mimsImagesDto = new MimsImagesDto();

            XSSFRow rowCurrent = worksheetExcelFile.getRow(i);
            System.out.println("ROW KE: " + i);

            String productNameExcelFile = rowCurrent.getCell(5).getStringCellValue();
            String pedaftarExcelFile = rowCurrent.getCell(10).getStringCellValue().split("-")[0].trim();
            String fileNameFolder = String.valueOf(i);

            mimsImagesDto.setNo(String.valueOf(i));
            mimsImagesDto.setNoReg((null != rowCurrent.getCell(0)) ? rowCurrent.getCell(0).getStringCellValue() : "");
            mimsImagesDto.setTglTerbit((null != rowCurrent.getCell(1)) ? rowCurrent.getCell(1).getStringCellValue() : "");
            mimsImagesDto.setMasaBerlaku((null != rowCurrent.getCell(2)) ? rowCurrent.getCell(2).getStringCellValue() : "");

            mimsImagesDto.setProduk((null != rowCurrent.getCell(4)) ? rowCurrent.getCell(4).getStringCellValue() : "");
            mimsImagesDto.setNamaProduk((null != rowCurrent.getCell(5)) ? rowCurrent.getCell(5).getStringCellValue() : "");
            mimsImagesDto.setBentukSediaan((null != rowCurrent.getCell(6)) ? rowCurrent.getCell(6).getStringCellValue() : "");
            mimsImagesDto.setKomposisi((null != rowCurrent.getCell(7)) ? rowCurrent.getCell(7).getStringCellValue() : "");

            mimsImagesDto.setKemasan((null != rowCurrent.getCell(9)) ? rowCurrent.getCell(9).getStringCellValue() : "");
            mimsImagesDto.setPendaftar((null != rowCurrent.getCell(10)) ? rowCurrent.getCell(10).getStringCellValue() : "");
            mimsImagesDto.setDiproduksiOleh((null != rowCurrent.getCell(11)) ? rowCurrent.getCell(11).getStringCellValue() : "");


            List<String> images = new ArrayList<>();
            String pathImage = pathImages.concat("\\").concat(fileNameFolder);
//            int countImage = 1;
            int count = 0;
            int maxTries = 2;
            boolean condition = true;
            while (condition) {
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(pathImage))) {
                    for (Path path : stream) {
//                    if (countImage >= 6) {
//                        break;
//                    }
                        if (!Files.isDirectory(path)) {
//                        images.add(path.toAbsolutePath().toString());
                            images.add("images\\".concat(fileNameFolder).concat("\\").concat(path.getFileName().toString()));

//                        countImage++;
                        }
                    }
                    if (++count != maxTries) {
                        break;
                    }
                } catch (Exception e1) {
                    fileNameFolder = productNameExcelFile.concat(" ").concat(pedaftarExcelFile);
                    pathImage = pathImages.concat("\\").concat(fileNameFolder);
                    condition = ++count != maxTries;
                }
            }

            mimsImagesDto.setImages(images);
            mimsImagesDtoList.add(mimsImagesDto);
        }

        Map<String, Object> model = new HashMap<>();

        int dtoSize = mimsImagesDtoList.size() / 1500;
        int lastIndex = 1500;

        for (int k = 0; k < dtoSize; k++) {
            List<MimsImagesDto> mimsImagesDtoShow;
            if (k == 0) {
                mimsImagesDtoShow = mimsImagesDtoList.subList(k, lastIndex);
            } else if (k == dtoSize - 1) {
                mimsImagesDtoShow = mimsImagesDtoList.subList(lastIndex, mimsImagesDtoList.size());
            } else {
                mimsImagesDtoShow = mimsImagesDtoList.subList(lastIndex, lastIndex + 1500);
                lastIndex = lastIndex + 1500;
            }

            model.put("products", mimsImagesDtoShow);

            generateFreemarkerDto.setModel(model);

            String generateBpomWithImage = printService.generateMims(generateFreemarkerDto);
//            System.out.println(generateBpomWithImage);

            BufferedWriter writer = new BufferedWriter(new FileWriter("bpom with image-" + k + ".html"));
            writer.write(generateBpomWithImage);

            writer.close();
        }
    }
}
