package com.gdt.similaritymims.dto;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

public class MimsReqDto {

    @NotBlank
    private BigDecimal threshold;

    @NotBlank
    private MultipartFile currentFile;

    @NotBlank
    private MultipartFile mimsFile;

    public BigDecimal getThreshold() {
        return threshold;
    }

    public void setThreshold(BigDecimal threshold) {
        this.threshold = threshold;
    }

    public MultipartFile getCurrentFile() {
        return currentFile;
    }

    public void setCurrentFile(MultipartFile currentFile) {
        this.currentFile = currentFile;
    }

    public MultipartFile getMimsFile() {
        return mimsFile;
    }

    public void setMimsFile(MultipartFile mimsFile) {
        this.mimsFile = mimsFile;
    }
}
