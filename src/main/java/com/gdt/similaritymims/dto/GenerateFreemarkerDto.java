package com.gdt.similaritymims.dto;

import java.util.Date;
import java.util.Map;

public class GenerateFreemarkerDto {

    private String contentType;

    private Map<String, Object> model;

    public GenerateFreemarkerDto() {
        contentType = "text/plain";
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Date getMailSendDate() {
        return new Date();
    }

    public Map<String, Object> getModel() {
        return model;
    }

    public void setModel(Map<String, Object> model) {
        this.model = model;
    }

}
