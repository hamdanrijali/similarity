package com.gdt.similaritymims.dto;

import java.util.List;

public class MimsImagesDto {
    private String no;
    private String noReg;
    private String tglTerbit;
    private String masaBerlaku;
    private String produk;
    private String namaProduk;
    private String pendaftar;
    private String diproduksiOleh;
    private String bentukSediaan;
    private String komposisi;
    private String kemasan;
    private List<String> images;

    public MimsImagesDto() {
    }

    public MimsImagesDto(String noReg, String tglTerbit, String masaBerlaku, String produk, String namaProduk, String pendaftar, String diproduksiOleh, List<String> images) {
        this.noReg = noReg;
        this.tglTerbit = tglTerbit;
        this.masaBerlaku = masaBerlaku;
        this.produk = produk;
        this.namaProduk = namaProduk;
        this.pendaftar = pendaftar;
        this.diproduksiOleh = diproduksiOleh;
        this.images = images;
    }

    public String getNoReg() {
        return noReg;
    }

    public void setNoReg(String noReg) {
        this.noReg = noReg;
    }

    public String getTglTerbit() {
        return tglTerbit;
    }

    public void setTglTerbit(String tglTerbit) {
        this.tglTerbit = tglTerbit;
    }

    public String getMasaBerlaku() {
        return masaBerlaku;
    }

    public void setMasaBerlaku(String masaBerlaku) {
        this.masaBerlaku = masaBerlaku;
    }

    public String getProduk() {
        return produk;
    }

    public void setProduk(String produk) {
        this.produk = produk;
    }

    public String getNamaProduk() {
        return namaProduk;
    }

    public void setNamaProduk(String namaProduk) {
        this.namaProduk = namaProduk;
    }

    public String getPendaftar() {
        return pendaftar;
    }

    public void setPendaftar(String pendaftar) {
        this.pendaftar = pendaftar;
    }

    public String getDiproduksiOleh() {
        return diproduksiOleh;
    }

    public void setDiproduksiOleh(String diproduksiOleh) {
        this.diproduksiOleh = diproduksiOleh;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getBentukSediaan() {
        return bentukSediaan;
    }

    public void setBentukSediaan(String bentukSediaan) {
        this.bentukSediaan = bentukSediaan;
    }

    public String getKomposisi() {
        return komposisi;
    }

    public void setKomposisi(String komposisi) {
        this.komposisi = komposisi;
    }

    public String getKemasan() {
        return kemasan;
    }

    public void setKemasan(String kemasan) {
        this.kemasan = kemasan;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }
}
