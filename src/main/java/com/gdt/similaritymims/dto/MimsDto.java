package com.gdt.similaritymims.dto;

public class MimsDto {
    private String productName;
    private String priceSell;
    private String priceSellPpn;
    private String pcs;
    private String similarity;
    private String rowMims;

    public MimsDto() {
    }

    public MimsDto(String productName, String priceSell, String priceSellPpn, String pcs, String similarity, String rowMims) {
        this.productName = productName;
        this.priceSell = priceSell;
        this.priceSellPpn = priceSellPpn;
        this.pcs = pcs;
        this.similarity = similarity;
        this.rowMims = rowMims;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPriceSell() {
        return priceSell;
    }

    public void setPriceSell(String priceSell) {
        this.priceSell = priceSell;
    }

    public String getPriceSellPpn() {
        return priceSellPpn;
    }

    public void setPriceSellPpn(String priceSellPpn) {
        this.priceSellPpn = priceSellPpn;
    }

    public String getPcs() {
        return pcs;
    }

    public void setPcs(String pcs) {
        this.pcs = pcs;
    }

    public String getSimilarity() {
        return similarity;
    }

    public void setSimilarity(String similarity) {
        this.similarity = similarity;
    }

    public String getRowMims() {
        return rowMims;
    }

    public void setRowMims(String rowMims) {
        this.rowMims = rowMims;
    }
}
